#!/usr/bin/env bash

set -ex

docker build -f ./deploy/api/Dockerfile -t local/c51/api:latest ./deploy/api
# docker build -f ./deploy/app/Dockerfile -t local/c51/app:latest ./deploy/app