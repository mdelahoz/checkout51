'use strict';

const express = require('express');

// Constants
const PORT = 80;
const HOST = '0.0.0.0';

// App
const app = express();
const fs = require("fs");

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('/', (req, res) => {
  res.send('Hello world\n');
});

//list offers
app.get('/listOffers', function (req, res) {
    fs.readFile( __dirname + "/" + "c51.json", 'utf8', function (err, data) {
        console.log( data );
        res.end( data );
    });
})

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
