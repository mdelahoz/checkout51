
#Checkout51 coding challenge
##Docker Setup & Installation
 1. Clone the repo
 2. `./develop.sh up -d` to start the api and app containers.
 
 
##Build Specs
 The system is built on NodeJS 8 for the api using c51.json file with VueJS for client end.

##Authors
 - Miguel De La Hoz
 
##Notes
1. API container run in localhost/listoffers
2. APP container run in localhost:8080
