#!/usr/bin/env bash

export API_PORT=${API_PORT:-80}

export APP_PORT=${APP_PORT:-8080}

# Decide which compose command & docker-compose file to use
COMPOSE="docker-compose -f ./deploy/docker-compose.dev.yml"


if [ $# -gt 0 ];then
    $COMPOSE "$@"
else
    $COMPOSE ps
fi